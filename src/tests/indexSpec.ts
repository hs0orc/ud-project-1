/*

import newArr from '..\\index';
import strings from '..\\strings';
import numbers from '..\\numbers';
import arrays from '..\\arrays';

const numArr = [3, 4, 5, 6];
const wordArr = ['cat', 'dog', 'rabbit', 'bird'];

it('should make a new array containing dog', () => {
  expect(newArr(3, wordArr)).toContain('dog');
});
it('make a new array containing 3', () => {
  expect(newArr(3, wordArr)).toContain(3);
});


it('should capitalize a string', () => {
  expect(strings.capitalize('a sentence')).toEqual('A Sentence');
});
it('should allow sentence to remain capitalized', () => {
  expect(strings.capitalize('A Sentence')).toEqual('A Sentence');
});


it('should be a sum greater than 10', () => {
  expect(numbers.sum(3,10)).toBeGreaterThan(10);
});
it('should be a sum less than 10', () => {
  expect(numbers.sum(-3,10)).toBeLessThan(10);
});

it('should multiply 3 by 5 and be 15', () => {
  expect(numbers.multiply(3,5)).toBe(15);
});
it('should multiply 0 by 5 to be falsy', () => {
  expect(numbers.multiply(0,5)).toBeFalsy();
});


it('should add numbers in array and be truthy', () => {
  expect(arrays.addArr(numArr)).toBeTruthy();
});
it('should add numbers in array and be 19', () => {
  expect(arrays.addArr(numArr)).toBe(19);
});

it('should concatinate 2 arrays to not equal the first', () => {
  expect(arrays.concatArr(numArr, wordArr)).not.toEqual(numArr);
});
it('should concatinate 2 arrays to not equal the second', () => {
  expect(arrays.concatArr(numArr, wordArr)).not.toEqual(wordArr);
});

it('should contain 3 items except rabbit', () => {
  expect(arrays.cut3(wordArr)).toEqual(['cat', 'dog', 'bird']);
});
it('should not contain the third index rabbit', () => {
  expect(arrays.cut3(wordArr)).not.toContain('rabbit');
});

it('should have 6 be largest number', () => {
  expect(arrays.lgNum(numArr)).toEqual(6);
});
it('should not have a large number and be falsy', () => {
  expect(arrays.lgNum(wordArr)).toBeFalsy();
});


/!*import arrays from '..\\arrays.js';
import numbers from '..\\numbers.js';
import strings from '..\\strings.js';

import countries from "../index";
*!/

/!*it("should get basic data on the country canada", async () => {
  const data = await countries.getCountry('canada');
  expect(data).toEqual([{
    capital: 'Ottawa',
    region: 'Americas',
    numericCode: '124'
  }]);
});*!/

/!*
it("getregionblock ", async () => {
  const data = await countries.getCountry('EU');
  expect(data).toEqual([{
    first: 'Åland Islands',
    second: 'Austria',
    third: 'Belgium'
  }]);
});
*!/

/!** Add test for getRegionCountries function here *!/

/!*
it("should get capitals of NAFTA countries", async () => {
  const data = await countries.getRegionCapitals('nafta');
  expect(data).toEqual([
    'Ottawa', 'Mexico City', 'Washington, D.C.'
  ]);
});
=======
/!*
import newArr from '../index';
import strings from '../strings';
import numbers from '../numbers';
import arrays from '../arrays';


const numArr = [3, 4, 5, 6];
const wordArr = ['cat', 'dog', 'rabbit', 'bird'];

describe("testing", () => {
  it('should make a new array containing dog', () => {
    expect(newArr(3, wordArr)).toContain('dog');
  });
  it('make a new array containing 3', () => {
    expect(newArr(3, wordArr)).toContain(3);
  });


  it('should capitalize a string', () => {
    expect(strings.capitalize('a sentence')).toEqual('A Sentence');
  });
  it('should allow sentence to remain capitalized', () => {
    expect(strings.capitalize('A Sentence')).toEqual('A Sentence');
  });


  it('should be a sum greater than 10', () => {
    expect(numbers.sum(3, 10)).toBeGreaterThan(10);
  });
  it('should be a sum less than 10', () => {
    expect(numbers.sum(-3, 10)).toBeLessThan(10);
  });

  it('should multiply 3 by 5 and be 15', () => {
    expect(numbers.multiply(3, 5)).toBe(15);
  });
  it('should multiply 0 by 5 to be falsy', () => {
    expect(numbers.multiply(0, 5)).toBeFalsy();
  });


  it('should add numbers in array and be truthy', () => {
    expect(arrays.addArr(numArr)).toBeTruthy();
  });

  it('should add numbers in array and be 18', () => {
    expect(arrays.addArr(numArr)).toBe(18);
  });

  it('should concatinate 2 arrays to not equal the first', () => {
    expect(arrays.concatArr(numArr, wordArr)).not.toEqual(numArr);
  });
  it('should concatinate 2 arrays to not equal the second', () => {
    expect(arrays.concatArr(numArr, wordArr)).not.toEqual(wordArr);
  });

  it('should contain 3 items except rabbit', () => {
    expect(arrays.cut3(wordArr)).toEqual(['cat', 'dog', 'bird']);
  });
  it('should not contain the third index rabbit', () => {
    expect(arrays.cut3(wordArr)).not.toContain('rabbit');
  });

  it('should have 6 be largest number', () => {
    expect(arrays.lgNum(numArr)).toEqual(6);
  });
  it('should not have a large number and be falsy', () => {
    expect(arrays.lgNum(wordArr)).toBeFalsy();
  });
});
*!/

// ----------------------------------------------------------


/!*
import myFunc from '..\\index';
import arrays from '..\\arrays';
import numbers from '..\\numbers.ts';
import strings from '..\\strings.ts';
>>>>>>> tmp

*!/

<<<<<<< HEAD
// ---------------------------------------------
=======
arrays.cut3([1,2,3,4,5]);
>>>>>>> tmp



/!*
describe(“suite description”, () => {
  it(“describes the spec”, () => {
    const myVar = true;
    expect(myVar).toBe(true);
  });
});
*!/


/!*it('expect myFunc(5) to equal 25', () => {
  expect(myFunc(5)).toEqual(25);
});*!/

/!*


it('expect subract(6, 1) to equal 5', () => {
  expect(numbers.subtract(6, 1)).toEqual(5);
});

it('expect multiply(2, 2) to equal 4', () => {
  expect(numbers.multiply(2, 2)).toBeGreaterThanOrEqual(4);
});

it('expect sum to equal 10', () => {
  expect(numbers.sum(5, 5)).toBeCloseTo(10, -1);
});

it('expect strings to Dog Walking', () => {
  expect(strings.capitalize('dog walking')).toMatch('Dog Walking');
});

it('largest num in array is 6', () => {
  expect(arrays.lgNum([4,5,6])).toBeLessThanOrEqual(6);
});

/!*it('third number in array is less than 8', () => {
  expect(arrays.cut3([4,5,6, 7, 8])).toBeLessThan(10);
});

it('con cat string dog running is dogrunning', () => {
  expect(strings.concat("dog", "running")).toEqual("dogrunning");
});
*!/


it('add two numbers in arr to be less than 5', () => {
  expect(arrays.addArr([1,2])).toBeLessThan(5);
});

*!/











*/
