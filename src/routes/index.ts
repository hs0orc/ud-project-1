import express from 'express';
import image from './api/image';
// import students from './api/students';
const routes = express.Router();

routes.get('/', (req, res) => {
  res.send('main api route');
});
routes.use('/images', image);
//routes.use('/students', students);
export default routes;

