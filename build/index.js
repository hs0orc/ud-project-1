"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//import arrays from 'arrays.js';
var arrays_1 = __importDefault(require("./arrays"));
var numbers_1 = __importDefault(require("./numbers"));
var strings_1 = __importDefault(require("./strings"));
/*import { cut3 } from "./arrays";

cut3([12,3,4,5,6]);*/
var numArr = [3, 4, 5, 6];
var wordArr = ['cat', 'dog', 'rabbit', 'bird'];
var arrSum = arrays_1.default.addArr(numArr);
var mixArr = arrays_1.default.concatArr(numArr, wordArr);
var myNum = '15' % 2;
var five = parseInt('5');
var newArr = function (num, arr) {
    return __spreadArray([num], arr);
};
console.log(arrays_1.default.cut3([1, 2, 3, 4, 5]));
console.log(numbers_1.default.sum(arrSum, myNum));
console.log(strings_1.default.capitalize('the quick brown fox'));
console.log(numbers_1.default.multiply(five, 8));
console.log(arrays_1.default.lgNum(mixArr));
exports.default = newArr;
//console.log("EXECUTED");
/*const myFunc = (num: number): number => {
  return num * num;
};

export default myFunc;*/
/*
class Person {

  private name: string;
  private age: number;
  private salary: number;

  constructor(name: string, age: number, salary: number) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  setName(name: string): void {
      this.name = name;
      console.log("set", this.name);
}

  getName(): string {
    console.log("get", this.name);
    return this.name;
  }

}

const something = new Person("aname", 15, 20000);
something.getName();
something.setName("china");

*/
