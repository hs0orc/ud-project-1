"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var arrays_js_1 = __importDefault(require("arrays.js"));
var numbers_js_1 = __importDefault(require("numbers.js"));
var strings_js_1 = __importDefault(require("strings.js"));
var numArr = [3, 4, 5, 6];
var wordArr = ['cat', 'dog', 'rabbit', 'bird'];
var arrSum = arrays_js_1.default.addArr(numArr);
var mixArr = arrays_js_1.default.concatArr(numArr, wordArr);
var myNum = '15' % 2;
var five = parseInt('5');
var newArr = function (num, arr) {
    return __spreadArray([num], arr);
};
console.log(newArr(3, wordArr));
console.log(arrays_js_1.default.cut3(mixArr));
console.log(numbers_js_1.default.sum(arrSum, myNum));
console.log(strings_js_1.default.capitalize('the quick brown fox'));
console.log(numbers_js_1.default.multiply(five, 8));
console.log(arrays_js_1.default.lgNum(mixArr));
exports.default = newArr;
/* Use Axios to get data from restcountries api
import axios from 'axios';

/!** Use the free API https://restcountries.eu/
 * You will use the following endpoints
 * https://restcountries.eu/rest/v2/name/{name} for countries by name
 * https://restcountries.eu/rest/v2/regionalbloc/{regionalbloc} for region blocks
 *!/

/!** Create getCountry Function here *!/
//const getCountry = async (name: string): Promise<string> => {
async function getCountry(name: string) {
  //const namer:string = name;
  const getApi = await axios(
    `https://restcountries.eu/rest/v2/name/${name}`
  );
  const data = getApi.data;
  //console.log('DATA', data);
  //
  const countries = [];
  for (let i = 0; i < data.length; i++) {
    countries.push(data[i].name);
    countries.push(data[i].capital);
    countries.push(data[i].region);
    countries.push(data[i].numericCode);

    //console.log("DATA COUNTRIES", data[i].name);
  }
  console.log('Country', countries);

  return countries;
  //return (countries: unknown) as string;
}

getCountry('canada');

/!** Create a test for this getRegion function *!/
async function getRegionCountries(regionalbloc: string) {
  const getApi = await axios(
    `https://restcountries.eu/rest/v2/regionalbloc/${regionalbloc}`
  );
  const data = getApi.data;
  const countries = [];
  for (let i = 0; i < data.length; i++) {
    countries.push(data[i].name);
  }
  console.log("getRegion Function, regional bloc", countries);
  return countries;
}

//const regionCountry: unknown = getRegionCountries("EU");

/!** Create getRegionCapitals function here *!/

async function getRegionCapitals(regionalbloc: string) {
  const getApi = await axios(
    `https://restcountries.eu/rest/v2/regionalbloc/${regionalbloc}`
  );
  const data = getApi.data;
  const countries = [];
  for (let i = 0; i < data.length; i++) {
    countries.push(data[i].capital);
  }
  console.log("getRegionCapital Function from regional bloc", countries);
  return countries;
}

//getRegionCapitals("nafta");
//console.log("getRegion Function, regional bloc", countries);

export default {
  getCountry,
  getRegionCountries,
  //getRegionCapitals
};
 */
//---------------------------------------------------------------
/*const myFunc = (num: number): number => {
  return num * num;
};

export default myFunc;*/
//---------------------------------------------------------------
/*
class Person {

  private name: string;
  private age: number;
  private salary: number;

  constructor(name: string, age: number, salary: number) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  setName(name: string): void {
      this.name = name;
      console.log("set", this.name);
}

  getName(): string {
    console.log("get", this.name);
    return this.name;
  }

}

const something = new Person("aname", 15, 20000);
something.getName();
something.setName("china");

*/
