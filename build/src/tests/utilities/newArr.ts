

const newArr = (num: number, arr:(string|number)[]): (string|number)[]=> {
  return [num, ...arr];
}

export default newArr;